# video_monitor_system

开发者安装方式：

1. 使用`virtualenv` 创建虚拟开发环境

2. 在Scripts目录下进行 `git clone` 

3. 进入虚拟环境，windows 下在Scripts目录下执行 `.\activate` 

   根据requirements.txt中的配置要求安装相应的库：`pip install -r requirements.txt `

